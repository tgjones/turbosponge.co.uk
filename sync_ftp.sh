#!/bin/bash

host=149.255.60.160
user=arqtanco
pass=

if [[ $pass == "" ]]; then
    echo -n FTP Password: 
    read -s pass
fi

local_root=$PWD/wwwroot
remote_root=/turbosponge
theme_dir=wp-content/themes/turbosponge

lftp -f "
open $host
user $user $pass
put -O $remote_root $local_root/.htaccess
mirror -R --delete --verbose $local_root/$theme_dir $remote_root/$theme_dir
bye
"
